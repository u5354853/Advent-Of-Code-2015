import Data.List
import Data.Char
{-
    HOW TO USE 
    ghc --make ./program
    cat input.txt | ./program
-}
main = do
    input <- getContents
    print $ paper input
    print $ ribbon input
-------- Functional Below Here ------------

paper :: String -> Int
paper input = sum.map (\[a,b,c] -> 2*a*b + 2*a*c + 2*b*c + a*b) $ (readInput input)

ribbon :: String -> Int
ribbon input = sum.map (\[a,b,c] -> 2*a + 2*b + a*b*c) $ (readInput input)

readInput :: String -> [[Int]]
readInput input = map (sort.parse) (lines input)

parse :: String -> [Int]
parse string = parse' string []
    where 
        parse' :: String -> String -> [Int]
        parse' string temp = case string of
            []      -> [read temp]
            'x':xs  -> (read temp):(parse' xs []) 
            x:xs    -> parse' xs (temp++[x])  