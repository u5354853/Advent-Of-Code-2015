import Data.List
import Data.Char

type Coord  = (Int,Int)
data Switch = On | Off | Toggle
type Inst   = (Switch,Coord,Coord)
-- O(n) lookup, oh well
type Grid   = [Row]
type Row    = [Bool]
type Grid2  = [Row2]
type Row2   = [Int] 
{-
    HOW TO USE 
    ghc --make ./program
    cat input.txt | ./program
-}

{-
 +RTS -K0 -RTS, NO LIMITS
 Seriously this is just plain bad code
-}
main = do
    input <- getContents
    print $ lights defaultGrid (map parse $ lines $ input)
    print $ brights defaultGrid2 (map parse $ lines $ input)

---------- Functional Below Here ------------ 

lights :: Grid -> [Inst] -> Int
lights grid input = case input of
    [] -> countOn grid
    x:xs -> lights (transistion grid x) xs

transistion :: Grid -> Inst -> Grid
transistion grid (switch,(x1,y1),(x2,y2)) = first ++ (map f second) ++ third
    where
        (first,rest)    = splitAt y1 grid
        (second,third)  = splitAt (y2-y1+1) rest
        
        f list = first' ++ (map g second') ++ third'
            where
                (first',rest') = splitAt x1 list 
                (second',third') = splitAt (x2-x1+1) rest' 
        
                g = case switch of
                    On -> (\x -> True)
                    Off -> (\x -> False)
                    Toggle -> not
-- way expensive
-- probs faster to just use (!!)
        
        
defaultGrid :: Grid
defaultGrid = replicate 1000 $ replicate 1000 False

countOn :: Grid -> Int
countOn grid = length.(filter id).concat $ grid

parse :: String -> Inst
parse input = case (words input) of
    ["turn","off",coord1,"through",coord2]  -> (Off,(parse' coord1),(parse' coord2)) 
    ["turn","on",coord1,"through",coord2]   -> (On,(parse' coord1),(parse' coord2))
    ["toggle",coord1,"through",coord2]      -> (Toggle,(parse' coord1),(parse' coord2))

parse' :: String -> Coord
parse' x = read $ '(':x++")"

---------------------------------------------------
-- I ain't doing this all over again, copy-paste --
---------------------------------------------------

brights :: Grid2 -> [Inst] -> Int
brights grid input = case input of
    [] -> sumBright grid
    x:xs -> brights (transistion2 grid x) xs

transistion2 :: Grid2 -> Inst -> Grid2
transistion2 grid (switch,(x1,y1),(x2,y2)) = first ++ (map f second) ++ third
    where
        (first,rest)    = splitAt y1 grid
        (second,third)  = splitAt (y2-y1+1) rest
        
        f list = first' ++ (map g second') ++ third'
            where
                (first',rest') = splitAt x1 list 
                (second',third') = splitAt (x2-x1+1) rest' 
        
                g = case switch of
                    On -> (\x -> x+1)
                    Off -> (\x -> if x==0 then 0 else x-1)
                    Toggle -> (\x -> x+2)
-- way expensive
-- probs faster to just use (!!)
        
        
defaultGrid2 :: Grid2
defaultGrid2 = replicate 1000 $ replicate 1000 0

sumBright :: Grid2 -> Int
sumBright grid = sum.concat $ grid
