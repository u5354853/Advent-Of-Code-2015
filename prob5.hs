import Data.List
import Data.Char
type Coord = (Int,Int)

{-
    HOW TO USE 
    ghc --make ./program
    cat input.txt | ./program
-}
main = do
    input <- getContents
    print $ sum $ map nice1 (lines input)
    print $ sum $ map nice2 (lines input)

-------- Functional Below Here ------------
nice1 :: String -> Int
nice1 string = fromEnum $ first && second && (not third)
    where
        first = (sum $ map fromEnum $ map (`elem` "aeiou") string) >= 3
        second = or $ map (\(x,y) -> x==y) double
        third = or $ map (\t -> (fst t):[snd t] `elem` ["ab","cd","pq","xy"]) double
        double = zip (tail string) (init string) 

nice2 :: String -> Int
nice2 string = fromEnum $ (fourth string) && fifth
    where 
        fifth = or $ map (\(x,y) -> x==y) double2
        double2 = zip (take n string) (drop 2 string)
        n = (length string)-2
        
fourth :: String -> Bool
fourth string = case string of
    x:y:xs
        |isInfixOf [x,y] xs -> True
        |otherwise -> fourth (y:xs)
    _ -> False