import Data.List
import Data.Char
{-
    HOW TO USE 
    ghc --make ./program
    cat input.txt | ./program
-}
main = do
    input <- getContents
    print $ part1 input
    print $ part2 input

-------- Functional Below Here ------------

part1 :: String -> Int
part1 input = total input 0
    where
        total :: String -> Int -> Int
        total input acc = case input of
            []      -> acc
            '(':xs  -> total xs (acc+1)
            ')':xs  -> total xs (acc-1)

part2 :: String -> Int
part2 input = findNeg input 0 0
    where
        findNeg :: String -> Int -> Int -> Int
        findNeg input floor index
            |floor < 0 = index
            |otherwise = case input of
                []      -> error "never reach floor"
                '(':xs  -> findNeg xs (floor+1) (index+1)
                ')':xs  -> findNeg xs (floor-1) (index+1)
