import Data.List
import Data.Char
type Coord = (Int,Int)

{-
    HOW TO USE 
    ghc --make ./program
    cat input.txt | ./program
-}
main = do
    input <- getContents
    print $ houses input [(0,0)] (0,0)
    print $ robot input [(0,0)] (0,0) (0,0) True
-------- Functional Below Here ------------

houses :: String -> [Coord] -> Coord -> Int
houses string coords pos = case string of
    [] -> length $ nub coords
    x:xs -> houses xs (newcoord:coords) newcoord
        where
            newcoord = move pos x

robot :: String -> [Coord] -> Coord -> Coord -> Bool -> Int
robot string coords santa_pos robot_pos santaTurn = case string of
    [] -> length $ nub coords
    x:xs
        |santaTurn -> robot xs (newsanta:coords) newsanta robot_pos (not santaTurn)
        |otherwise -> robot xs (newrobot:coords) santa_pos newrobot (not santaTurn)
            where
                newrobot = move robot_pos x
                newsanta = move santa_pos x 

move :: Coord -> Char -> Coord
move (x,y) dict = case dict of
    '^' -> (x,y+1)
    'v' -> (x,y-1)
    '<' -> (x-1,y)
    '>' -> (x+1,y)


